# Notes on AI Interpretability and Alignment

Suggestions welcome! Reach out [@regressorjourn](https://twitter.com/regressorjourn)

Will be adding more soon!

## Start Here

## Links
- [200 Concrete Open Problems in Mechanistic Interpretability by Neel Nanda](https://www.lesswrong.com/posts/LbrPTJ4fmABEdEnLf/200-concrete-open-problems-in-mechanistic-interpretability)

### Per-topic

Visualization

- Attention heads (...)

## Open-source Projects

## Forums, Blogs, Platforms

## Boot camps, fellowships, etc.
- [ML Alignment & Theory Scholars (MATS)](https://www.matsprogram.org)
- [Supervised Program for Alignment Research (SPAR)](https://supervisedprogramforalignment.org)




### Online

### In-person

## Career Opportunities and Advice

## Communities
